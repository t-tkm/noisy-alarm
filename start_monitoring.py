#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
import RPi.GPIO as GPIO
import time
import commands
import os
import re
import sys
 
NOISY_LEVEL_THRESHOLD = 500
ALERM_TIME_RANGE = 5 #second
GPIO_PIN = 17

GPIO.setmode(GPIO.BCM)
GPIO.setup(GPIO_PIN,GPIO.OUT)
GPIO.output(GPIO_PIN, False)

try:
    print("Start monitoring...")
    while True:
        cmd = "/home/pi/.local/bin/soundmeter -s 1 -c"
        output = commands.getoutput(cmd) 
        for line in output.split("\n"):
            m = re.match(r"^\s+(max):\s+(\d+)", line)
            if m is not None:
                max_monitoring_result = "\t".join(str(e) for e in [m.groups()[1]])
                print("Noisy level:" + max_monitoring_result)
                if int(max_monitoring_result) > NOISY_LEVEL_THRESHOLD:
                    print("Noisy!!!!")
                    GPIO.output(GPIO_PIN, True)
                    time.sleep(ALERM_TIME_RANGE)
                    GPIO.output(GPIO_PIN, False)
except KeyboardInterrupt:
    GPIO.cleanup()